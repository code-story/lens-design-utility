require 'bootstrap'
require 'prism.coffeescript'
require 'prism.jade'
require 'prism.markup'
require 'prism.scss'
require 'prism.line-numbers'

Router = require './router/router'
Backbone = require 'backbone'
Marionette = require 'marionette'

App = new Marionette.Application()

App.addRegions
    mainRegion: '#page'
    navRegion : '#links'
    bodyMainRegion: '.page'
    bodyDemoRegion: '.page-demo'

App.Router = new Router(App)

App.addInitializer ->
    Backbone.history.start()

module.exports = App