Backbone = require 'backbone'

class Router extends Backbone.Router
    initialize: (@app) ->
    routes:
        ''       : 'showDemo'
        'demo'   : 'showDemo'
        'story'  : 'showStory'

    showDemo   : -> @app.trigger('demo:requested')
    showStory  : -> @app.trigger('story:requested')

module.exports = Router