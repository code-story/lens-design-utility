require './pages/nav'
require './pages/demo'
require './pages/story'
require './pages/backlog'

$ = require 'jquery'
App = require './app'

$ -> App.start()