App = require '../app'
Marionette = require 'marionette'

# ####################################################
# VIEWS
# ####################################################

class NavView extends Marionette.ItemView
    template: '#nav-template'
    events:
        'click #nav-backlog': 'toggleBacklog'

    toggleBacklog: (evt) ->
        evt.preventDefault()
        App.trigger('backlog:toggle')
        @isBacklogVisible = not @isBacklogVisible
        @$('#nav-backlog').text((if @isBacklogVisible then 'Hide' else 'Show') + ' Backlog')

# ####################################################
# CONTROLLERS
# ####################################################

class NavController extends Marionette.Controller
    initialize: ->
        App.navRegion.show new NavView()

module.exports = new NavController()