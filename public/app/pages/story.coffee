App = require '../app'
Utils = require '../utils'
Marionette = require 'marionette'

# ####################################################
# VIEWS
# ####################################################

class StoryView extends Marionette.ItemView
    template: '#home-template'

# ####################################################
# CONTROLLERS
# ####################################################

class StoryController extends Marionette.Controller
    initialize: ->
        App.on 'story:requested', => @show()

    show: ->
        @view = new StoryView()
        App.bodyMainRegion.$el.show()
        App.bodyDemoRegion.$el.hide()
        App.mainRegion.show @view
        App.Router.navigate 'story'
        Utils.initCodeSnippets()

module.exports = new StoryController()