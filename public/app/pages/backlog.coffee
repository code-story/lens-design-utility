App = require '../app'
Marionette = require 'marionette'

# ####################################################
# VIEWS
# ####################################################

class BacklogView extends Marionette.ItemView
    el: '.todo'

# ####################################################
# CONTROLLERS
# ####################################################

class BacklogController extends Marionette.Controller
    initialize: ->
        @view = new BacklogView()
        @view.$el.hide().removeClass('hidden')
        App.on 'backlog:toggle', => @view.$el.toggle()

module.exports = new BacklogController()