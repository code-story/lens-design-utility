App = require '../app'
$ = require 'jquery'
draggable = require 'draggable'
swal = require 'swal'
Marionette = require 'marionette'

# ####################################################
# VIEWS
# ####################################################

class LensView extends Marionette.ItemView
    template: '#lens-template'
    onShow: ->
        draggable(@$('.lens-circle')[0])

# ####################################################
# CONTROLLERS
# ####################################################

class DemoController extends Marionette.Controller
    initialize: ->
        App.on 'demo:requested', => @show()

        @imageIndex = localStorage['last-image-index'] or 0
        @images = [
            'screenshot1.png'
            'screenshot2.png'
        ]
        @updateImage()

        @isReflexVisible = localStorage['reflex-visible'] isnt 'false'
        @updateReflex()

        @sizeIndex = localStorage['last-size-index'] or 0
        @sizes = [
            '300px'
            '350px'
            '400px'
            '450px'
            '250px'
        ]
        @updateSize()

        @colorIndex = localStorage['last-border-color'] or 0
        @borderColors = [
            'white'
            '#FA4025'
            '#DB9A1E'
            'teal'
        ]
        @updateColor()

        @paddingIndex = localStorage['last-padding-index'] or 0
        @paddingOptions = [
            { left: 0, top: 0, right: 0, bottom: 0 }
            { left: '300px', top: '300px' }
            { right: '300px', top: '300px' }
            { right: '300px', bottom: '300px' }
            { left: '300px', bottom: '300px' }
        ]
        @updatePadding()

        @isBlur = localStorage['blur-mode'] isnt 'false'
        @updateClipBlur()

        $('body')
            .keypress (e) =>
                switch e.charCode
                    when 'b'.charCodeAt(0) then location.href = '/#story'
                    when 'n'.charCodeAt(0) then @chooseNextImage()
                    when 'p'.charCodeAt(0) then @choosePrevImage()
                    when 'r'.charCodeAt(0) then @toggleReflex()
                    when 's'.charCodeAt(0) then @chooseSize()
                    when 'c'.charCodeAt(0) then @chooseColor()
                    when 'd'.charCodeAt(0) then @choosePadding()
                    when 'm'.charCodeAt(0) then @toggleClipBlur()
                    when '?'.charCodeAt(0) then @printHelp()

    printHelp: ->
        swal
            title: 'Shortcuts'
            text: '''<pre style="text-align: left">
            'b' => Go back to the article

            'r' => Toggle reflex inside the len
            'n' => Choose next image
            'p' => Choose previous image
            's' => Toggle len size
            'c' => Toggle len border color
            'd' => Shift pane to allow hovering image border
            'm' => Toggle blur / clip mode

            '?' => Help
            </pre>'''
            html: true

    chooseNextImage: ->
        @imageIndex = (@imageIndex + 1) % @images.length
        @updateImage()
    choosePrevImage: ->
        @imageIndex = (@imageIndex + @images.length - 1) % @images.length
        @updateImage()
    updateImage: ->
        localStorage['last-image-index'] = @imageIndex
        image = '/dist/images/' + @images[@imageIndex]
        $('head').append """
        <style>
            .lens-image,
            .lens-circle {
                background-image: url(#{image});
            }
        </style>
        """
    toggleReflex: ->
        @isReflexVisible = not @isReflexVisible
        @updateReflex()
    updateReflex: ->
        localStorage['reflex-visible'] = @isReflexVisible
        shadowForReflex = '0 0 50px rgba(17, 17, 17, 0.8), inset 0 0 4px rgba(17, 17, 17, 0.4)'
        shadowWithoutReflex = '0 0 50px rgba(17, 17, 17, 0.8), inset 0 0 40px 2px rgba(0, 0, 0, 0.25)'
        $('head').append """
        <style>
            .lens-circle:after {
                content: #{if @isReflexVisible then '""' else 'none' };
            }
            .lens-circle {
                box-shadow: #{if @isReflexVisible then shadowForReflex else shadowWithoutReflex };
            }
        </style>
        """
    chooseSize: ->
        @sizeIndex = (@sizeIndex + 1) % @sizes.length
        @updateSize()
    updateSize: ->
        localStorage['last-size-index'] = @sizeIndex
        size = @sizes[@sizeIndex]
        $('head').append """
        <style>
            .lens-circle {
                width: #{size};
                height: #{size};
            }
        </style>
        """
    chooseColor: ->
        @colorIndex = (@colorIndex + 1) % @borderColors.length
        @updateColor()
    updateColor: ->
        localStorage['last-border-color'] = @colorIndex
        color = @borderColors[@colorIndex]
        $('head').append """
        <style>
            .lens-circle {
                border-color: #{color};
            }
        </style>
        """
    choosePadding: ->
        @paddingIndex = (@paddingIndex + 1) % @paddingOptions.length
        @updatePadding()
    updatePadding: ->
        localStorage['last-padding-index'] = @paddingIndex
        padding = @paddingOptions[@paddingIndex]
        $('head').append """
        <style>
            .lens-image,
            .lens-circle {
                background-position:
                    #{padding.left or ('-' + padding.right)}
                    #{padding.top or ('-' + padding.bottom)}
            }
        </style>
        """
    toggleClipBlur: ->
        @isBlur = not @isBlur
        @updateClipBlur()
    updateClipBlur: ->
        localStorage['blur-mode'] = @isBlur
        $('head').append """
        <style>
            .lens-image {
                opacity: #{if @isBlur then '1' else '0' };
            }
            body {
                background-size: #{if @isBlur then '6px 6px' else '0 0' };
            }
        </style>
        """

    show: ->
        @view = new LensView()
        App.bodyMainRegion.$el.hide()
        App.bodyDemoRegion.$el.show()
        App.bodyDemoRegion.show @view
        App.Router.navigate 'demo'

module.exports = new DemoController()