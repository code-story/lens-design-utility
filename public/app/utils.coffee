$ = require 'jquery'
Prism = require 'prism'
Marionette = require 'marionette'

class Utils extends Marionette.Object
    initCodeSnippets: ->
        $('pre > code').each (i , code) =>
            $code = $(code)
            $pre = $code.parent()
            [prefix,rest...] = $code.html().split('\n')
            [_, language, lines, height] = prefix.match('// ([a-z\-]+)( lines)?( [0-9]+px)?')

            if language is 'embed'
                $pre.after($code.text().split('\n')[1..].join('\n')).remove()
            else
                $code.html(rest.join('\n'))
                $pre.addClass('language-' + language)
                if lines  then $pre.addClass('line-numbers')
                if height then $code.attr('style', 'overflow: auto; display: block; max-height: ' + height)

        Prism.highlightAll()

module.exports = new Utils()