### Lens for screenshot focus design

[Skip to demo](#demo)

---

![](/images/annotating-in-snipping-tool.jpg)

---
        
I just can't stand these Paint-like drawings. Skitch is also overused. I think we 
can do it better. Circles are great in focusing readers on the featured features 
placement and design. The area outside is just the context, reference. To avoid 
dragging user attention away I suggest using blur or clipping the whole area out.

---

![](/images/screenshot_article.png)

---

> _There is more.._

Don't use this pattern blindly. This tool is giving you easy way to move away from 
arrows once for all, but you can also choose one of these similar patterns:

- markers ([example](http://www.imranyounis.com/public/img/skill/product-walkthrough.png))
- side-notes ([example](https://confluence.atlassian.com/jirasoftwareserver070/working-with-issues-762877828.html))
- clipping whole component ([example](http://media02.hongkiat.com/webapp-design-resources/mobile-web-app-dev.jpg))
- no annotations (you don't need them always, do you?)


Now.. If you're interested in the source code.. there is really not much to look at.

    // markup
    <div class="lens-image"></div>
    <div class="lens-circle"></div>    
    
---
    
    // scss 400px
    /*
        Credits to: Hugo Darby-Brown
        http://codepen.io/hugo/pen/jFzqm
        Greyscale Magnifying glass effect
    */
    
    $image: '/dist/images/screenshot1.png'; // initial image

    .lens-image {
        background: url($image) no-repeat;
        background-attachment: fixed;
        width: 1438px;
        height: 1500px;
        position: absolute;
        filter: blur(1px) grayscale(100%) sepia(10%);
        // I'll get rid of "-webkit" when I add autoprefixer to build pipeline
        -webkit-filter: blur(1px) grayscale(100%) sepia(10%); 
    }

    $initial-size: 350px;
    .lens-circle { 
        background: url($image) no-repeat;
        background-attachment: fixed;
        border-radius: 50%;
        border: 0.5em solid #DB9A1E;
        box-shadow: 0 0 50px rgba(#111,0.8),
              inset 0 0 4px rgba(#111,0.4);
        cursor: all-scroll;  
        height: $initial-size;
        margin: -7.5em 0 0 -7.5em;
        overflow: hidden; 
        position: absolute;
        top: 260px;
        left: 15%;
        width: $initial-size;

        &:after {
            background: linear-gradient(rgba(255,255,255,.6), transparent);
            border-radius: 50%;
            content: '';
            height: 50%;
            left: 10%;
            position: absolute;
            top: 5%;
            width: 80%;
        }
    }

---

The logic is trivial. We just make the circle draggable and bind some shortcuts.
    
    // coffeescript
    class LensView extends Marionette.ItemView
        template: '#lens-template'
        onShow: -> draggable(@$('.lens-circle')[0])

    # ShortcutRegister and Configuration classes will be introduced in 1.0.1 to get rid of the methods in demo.coffee and make it all DRY         
    class DemoController extends = Marionette.Controller
        initialize: ->
            ShortcutRegister
                'r': Configuration.toggle('reflex', 'Toggle reflex inside the len')
                'n': Configuration.chooseNext('image', 'Choose next image')
                'p': Configuration.choosePrev('image', 'Choose previous image')
                's': Configuration.chooseNext('size', 'Toggle len size')
                'c': Configuration.chooseNext('color', 'Toggle len border color')
                'd': Configuration.chooseNext('padding', 'Shift pane to allow hovering border of the image')
            
            # Register defaults (e.g. Configuration.defaults( imageIndex: 1, ).collections(image: [], ))
            # Set "config:*" event handlers (e.g. Configuration.on 'choose:image', ({value}) -> ...)
            