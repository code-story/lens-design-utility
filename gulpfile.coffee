st   = require 'st'
http = require 'http'
gulp = require 'gulp'
$    = require('gulp-load-plugins')()
es   = require 'event-stream'
source = require 'vinyl-source-stream'
buffer = require 'vinyl-buffer'
browserify = require 'browserify'
bundleLogger = require './tools/bundleLogger'

gulp.task 'coffee', ->
    gulp.src 'public/app/**/*.coffee'
        .pipe $.plumber()
        .pipe $.coffee(bare: true)
        .on 'error', $.util.log
        .pipe gulp.dest 'public/dist/app'
        # .pipe $.livereload()

gulp.task 'bundle', ->
    bundleLogger.start()
    browserify
            entries: ['public/app/start.coffee']
            extensions: ['.coffee']
        .bundle()
        .pipe source 'bundle.js'
        .pipe buffer()
        .pipe $.uglify()
        .pipe $.rename 'app.min.js'
        .pipe gulp.dest 'public/dist/app'
        .on 'end', bundleLogger.end

gulp.task 'sass', ->
	appStyles = gulp.src 'public/styles/**/*.scss'
        .pipe $.plumber()
        .pipe $.sass()
        .on 'error', $.util.log
		.pipe $.concat 'app.css'
		.pipe $.cssmin()

	libStyles = gulp.src [
        "public/vendor/bootstrap/dist/css/bootstrap.css"
        "public/vendor/sweetalert/lib/sweet-alert.css"
        "public/vendor/prism/themes/prism-tomorrow.css"
        "public/vendor/prism/plugins/line-numbers/prism-line-numbers.css"
		]
		.pipe $.cssmin()

	es.concat libStyles, appStyles
		.pipe $.concat 'styles.min.css'
        .pipe gulp.dest 'public/dist/styles'
        # .pipe $.livereload()

gulp.task 'jade', ->
    gulp.src 'public/index.jade'
        .pipe $.plumber()
        .pipe $.jade()
        .pipe gulp.dest 'public'
        # .pipe $.livereload()

gulp.task 'assets', ->
    gulp.src 'public/images/**/*'
        .pipe gulp.dest 'public/dist/images'

gulp.task 'watch', ['server'], ->
    # $.livereload.listen(basePath: 'public/dist')
    gulp.watch 'public/app/**/*.coffee'  , ['bundle']
    gulp.watch 'public/styles/**/*.scss' , ['sass']
    gulp.watch 'public/**/*.+(jade|md)'  , ['jade']

gulp.task 'server', (done) ->
  port = process.env.PORT or 8010
  if process.env.DEBUG
    $.util.log "Server started: http://localhost:#{port}/"

  http.createServer st
      path  : __dirname + '/public'
      index : 'index.html'
      cache : false
		.listen port, done
		

gulp.task 'build'  , ['bundle', 'sass', 'jade', 'assets']
gulp.task 'serve'  , ['build', 'server']
gulp.task 'default', ['build', 'watch']