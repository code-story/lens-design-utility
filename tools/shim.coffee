module.exports = {
    jquery:
        exports: '$'
    underscore:
        exports: '_'
    backbone:
        exports: 'Backbone'
        depends:
            jquery: '$'
            underscore: '_'
    marionette:
        exports: 'Marionette'
        depends:
            backbone: 'Backbone'
    bootstrap:
        depends:
            jquery: '$'
    swal:
        exports: 'swal'
    moment:
        exports: 'moment'
    draggable:
        exports: 'draggable'
    prism:
        exports: 'Prism'
    sortable:
        exports: 'Sortable'
    'prism.scss':         depends: prism: 'Prism'
    'prism.coffeescript': depends: prism: 'Prism'
    'prism.jade':         depends: prism: 'Prism'
    'prism.markup':       depends: prism: 'Prism'
    'prism.line-numbers': depends: prism: 'Prism'
}
