#[Lens Design Utility](https://bitbucket.org/code-story/lens-design-utility)

Prepare walkthrough screenshots using lens design

----

![Lens Design Utility](screenshots/promo.png)

----
Read the [story](https://lens-design-utility.herokuapp.com/#story)

See the [demo](https://lens-design-utility.herokuapp.com)

## Key features
- Help dialog on `?` shortcut
- Switching to next/previous image (`n`/`p`)
- Choosing border color (`c`), size (`s`), glass reflection (`r`)
- Toggle between blur / clip mode (`m`)
- Shortcut to go back to the article (`b`)

### TODO
- Upload images with GoogleDrive API
- Configurability, saving configurations on different urls
- Substitute screenshot utility (like Greenshot, Skitch or Shutter)
- Spike exporting PNG with transparent background (in clip mode)

### Nice to have
- Shift-drag background (for viewport smaller than image)

---

## Running locally

Build & start

```
> npm run-script prepare
> npm install
> npm start
```   

Navigate to [http://localhost:8010](http://localhost:8010)